import React, {Component} from 'react';
import axios from 'axios';

class Modif extends Component {

  constructor() {
    super();
    this.state = {
      reclamation: {
        dateReclamation: '',
        object: '',

      },
      to:"",
      content:"",

    };


  }
  componentDidMount() {
    console.log('id',localStorage.getItem(('id')));

    this.getone();


  }
  getone() {
    fetch('http://localhost:8080/reclamation/one/' + localStorage.getItem('id'),{method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({reclamation: data});
        this.setState({to: data['client']['email']});

      });
  }

reponse (){

  fetch('http://localhost:8080/reclamation/reponse/'+localStorage.getItem('id'),{method: 'GET'})
    .then(response => response.json())
    .then(data => {
      console.log(data);
    this.sendMail();
    });
}
sendMail(){

  axios.post("http://localhost:8080/reclamation/sendMail",({
    to:this.state.to,
    content:this.state.content
  }))
    .then(res=>{
      console.log("data",res.data);


     // window.location.href = '/Reclamation';
    });
}


  render() {
    return (
      <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
          <div class="login-content">
            <div class="login-logo">
              <a href="index.html">
                <img class="align-content" src="images/logo.png" alt=""/>
              </a>
            </div>
            <div class="modifierpharmacie">
            <div class="login-form">
              <div><h1>Reponse Reclamation</h1></div>
              <div class="form-group">
                <label>object</label>
                <input type="object" class="form-control" placeholder={this.state.reclamation.object}
                       onChange={evt=> this.setState({object: evt.target.value})}/>
              </div>
              <div class="form-group">
                <label>dateReclamation</label>
                <input type="dateReclamation" class="form-control" placeholder={this.state.reclamation.dateReclamation}
                       onChange={evt=> this.setState({dateReclamation: evt.target.value})}/>
              </div>

              <div><textarea placeholder="Message" onkeyup="adjust_textarea(this)"
              value={this.state.content}  onChange={evt=> this.setState({content: evt.target.value})}></textarea></div>


              <button type="submit" onClick={this.reponse.bind(this)} class="btn btn-primary btn-flat m-b-30 m-t-30">Send</button>

              <div class="register-link m-t-15 text-center">
                <p>Already have account ? <a href="#"> Sign in</a></p>
              </div>

            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default Modif;
