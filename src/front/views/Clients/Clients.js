// @flow weak

import React, {
  PureComponent,
}                       from 'react';
import PropTypes        from 'prop-types';
import { AnimatedView } from '../../components';

let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;

class SimpleTables extends PureComponent {
  constructor() {
    super();
    {
      this.state = {
        clients: [],
        currentPage: 1,
        todosPerPage: 5
      };
    }
    this.handleClick = this.handleClick.bind(this);

    this.handleLastClick = this.handleLastClick.bind(this);

    this.handleFirstClick = this.handleFirstClick.bind(this);
  }
  handleClick(event) {

    event.preventDefault();

    this.setState({
      currentPage: Number(event.target.id)
    });
  }



  handleLastClick(event) {

    event.preventDefault();

    this.setState({
      currentPage:last
    });
  }


  handleFirstClick(event) {

    event.preventDefault();

    this.setState({
      currentPage:1
    });
  }
  componentWillMount() {
    const { actions: {  enterSimpleTables } } = this.props;
    enterSimpleTables();
  }

  componentWillUnmount() {
    const { actions: {  leaveSimpleTables } } = this.props;
    leaveSimpleTables();
  }
  componentDidMount() {
    this.getAll();
  }


  getAll() {
    console.log("token ", localStorage.getItem("token"));
    const headers={
      "Authorization":"Bearer "+localStorage.getItem("token")
    }
  fetch('http://localhost:8080/client/all', {method: 'GET', headers: headers})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({clients: data});
      });
  }







  render() {
    let {clients, currentPage, todosPerPage} = this.state;


    // Logic for displaying current todos

    let indexOfLastTodo = currentPage * todosPerPage;

    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;

    let currentTodos = clients.slice(indexOfFirstTodo, indexOfLastTodo);


    prev = currentPage > 0 ? (currentPage - 1) : 0;

    last = Math.ceil(clients.length / todosPerPage);

    next = (last === currentPage) ? currentPage : currentPage + 1;



    // Logic for displaying page numbers

    let pageNumbers = [];

    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }
    return (
      <AnimatedView>

          <div className="row">
            <div className="col-xs-12">
              <div className="panel">
                <header className="panel-heading">
                  <p class="liste">Liste des Clients</p>
                </header>
                <div className="panel-body table-responsive">
                  <div className="box-tools m-b-15">
                    <div className="input-group">
                      <input
                        type="text"
                        name="table_search"
                        className="form-control input-sm pull-right"
                        style={{width: '150px'}}
                        placeholder="Search"
                      />
                      <div className="input-group-btn">
                        <button className="btn btn-sm btn-default">
                          <i className="fa fa-search" />
                        </button>
                      </div>
                    </div>
                  </div>
                  <table className="table table-hover">
                    <thead>
                      <tr>

                        <th>nom</th>
                        <th>prenom</th>
                        <th>adresse</th>
                        <th>email</th>
                        <th>tel</th>
                        <th>dateNaissance</th>
                        <th>ville</th>
                        <th>genre</th>


                      </tr>
                    </thead>
                    <tbody>
                    {
                    currentTodos.map((item,index) =>{

                      return(

                      <tr key={index}>
                        <td>{item.firstName}</td>
                        <td>{item.lastName}</td>
                        <td>{item.adresse}</td>
                        <td>{item.email}</td>
                        <td>{item.tel}</td>
                        <td>{item.dateN}</td>
                        <td>{item.genre}</td>
                        <td>{item.ville}</td>



                      </tr>
                      );

                    })

                    }
                    </tbody>
                  </table>
                </div>
                      <div className="table-foot">
                      <ul className="pagination">
                      <li className={prev === 0 ? 'disabled' : ''}>
                      <a onClick={this.handleFirstClick} id={prev} href={prev}>First</a>
                      </li>
                      <li className={prev === 0 ? 'disabled' : ''}>
                      <a onClick={this.handleClick} id={prev} href={prev}>Previous</a>
                      </li>
                    {pageNumbers.map((page, index) =>
                      <li key={index} className={pageNumbers[currentPage-1] === page ? 'active' : ''}>
                      <a PaginationLink onClick={this.handleClick} href={page} key={page} id={page}>{page}</a>
                      </li>
                      )}
                      <li className={currentPage === last ? 'disabled' : ''}>
                      <a onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Next</a>
                      </li>
                      <li className={currentPage === last? 'disabled' : ''}>
                      <a  onClick={this.handleLastClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Last</a>
                      </li>
                      </ul>



                      </div>
              </div>
            </div>
          </div>
      </AnimatedView>
    );
  }
}

SimpleTables.propTypes = {
  actions: PropTypes.shape({
    enterSimpleTables: PropTypes.func,
    leaveSimpleTables: PropTypes.func,
  }),
};

export default SimpleTables;
