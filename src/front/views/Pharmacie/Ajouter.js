import React, {Component} from 'react';
import axios from 'axios';
import Modal from 'react-responsive-modal';

class Modif extends Component {

  constructor() {
    super();
    this.state = {
      pharmacie: {
        nomPh: '',
        matricule: '',
        ville: '',
        adresse: '',
        localisation: '',
        email: '',
        password: '',
        username: '',
        tel: '',
        open: false,
      },

    };
  }

  onOpenModal = () => {
    this.setState({open: true});
  };
  onCloseModal = () => {
    this.setState({open: false});
  };


  register() {
    console.log("token ", localStorage.getItem("token"));
    const headers={
      "Authorization":"Bearer "+localStorage.getItem("token")
    }
    console.log("state: ", this.state)
    if (this.state.nomPh === "" && this.state.matricule === "" && this.state.ville === "" && this.state.adresse === "" && this.state.localisation===""&& this.state.email===""&&this.state.password===""
      &&this.state.username===""&&this.state.tel==="") {
      alert("no data");
    }
    else {
      axios.post("http://localhost:8080/pharmacie/add", {
        nomPh: this.state.nomPh,
        matricule: this.state.matricule,
        ville: this.state.ville,
        adresse: this.state.adresse,
        localisation: this.state.localisation,
        email: this.state.email,
        password: this.state.password,
        username: this.state.username,
        tel: this.state.tel,
      },{headers:headers}).then(res => {
        console.log(res.data);
        this.onOpenModal();
        window.location.href = "/Pharmacie"
      })
    }
    this.setState({nomPh: ""})
    this.setState({matricule: ""})
    this.setState({ville: ""})
    this.setState({adresse: ""})
    this.setState({localisation: ""})
    this.setState({email: ""})
    this.setState({password: ""})
    this.setState({username: ""})
    this.setState({tel: ""})

  }


  render() {
    const {open} = this.state;
    return (

        <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
          <div class="login-content">
            <div class="login-logo">
              <a href="index.html">
                <img class="align-content" src="images/logo.png" alt=""/>
              </a>
            </div>
            <div class="ajouterpharmacie">
              <div class="login-form">
                <div><h1>Ajouter Pharmacie</h1></div>
                <div class="form-group">

                  <p>NomPharmacie</p>
                  <input type="nomPh" class="form-control" placeholder=" nom"
                         onChange={evt => this.setState({nomPh: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>Matricule</p>
                  <input type="matricule" class="form-control" placeholder="matricule"
                         onChange={evt => this.setState({matricule: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>ville</p>
                  <input type="ville" class="form-control" placeholder="ville"
                         onChange={evt => this.setState({ville: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>AdressePharmacie</p>
                  <input type="adresse" class="form-control" placeholder="adresse"
                         onChange={evt => this.setState({adresse: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>Localisation</p>
                  <input type="localisation" class="form-control" placeholder="localisation"
                         onChange={evt => this.setState({localisation: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>email</p>
                  <input type="email" class="form-control" placeholder="email"
                         onChange={evt => this.setState({email: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>username</p>
                  <input type="username" class="form-control" placeholder="username"
                         onChange={evt => this.setState({username: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>password</p>
                  <input type="password" class="form-control" placeholder="password"
                         onChange={evt => this.setState({password: evt.target.value})}/>
                </div>

                <div class="form-group">
                  <p>tel</p>
                  <input type="tel" class="form-control" placeholder="tel"
                         onChange={evt => this.setState({tel: evt.target.value})}/>
                </div>


                <div class="btnAjouter">
                  <button type="Ajouter" onClick={this.register.bind(this)}
                          class="btn btn-primary btn-flat m-b-30 m-t-30">Ajouter</button>

                </div>
                <Modal open={open} onClose={this.onCloseModal} right>
                  <td>  <h6 class="div123"> pharmacie ajouté avec succées</h6></td>



                </Modal>
              </div>

            </div>
          </div>
        </div>
      </div>


    );
  }
}

export default Modif;
