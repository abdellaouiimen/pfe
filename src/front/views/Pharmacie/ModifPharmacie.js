import React, {Component} from 'react';
import axios from 'axios';

class Modif extends Component {

  constructor() {
    super();
    this.state = {
      pharmacie: {
        nomPh: '',
        matricule: '',
        ville: '',
        adresse: '',
        localisation: '',
        email: '',
        password: '',
        username: '',
        tel: '',

      },
      nomPh: '',
      matricule: '',
      ville: '',
      adresse: '',
      localisation: '',
      email: '',
      password: '',
      username: '',
      tel: '',

    };


  }
  componentDidMount() {
    console.log('id',localStorage.getItem(('id')));
    this.getone();


  }
  getone() {
    console.log("token ", localStorage.getItem("token"));
    const headers={
      "Authorization":"Bearer "+localStorage.getItem("token")
    }
    fetch('http://localhost:8080/pharmacie/one/' + localStorage.getItem('id'),{method: 'GET', headers: headers})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({pharmacie: data});
      });
  }
  componentWidMount() {
    console.log('will');

  }

  handleEdit() {
    console.log('state: ',this.state);

    if (this.state.nomPh === '') {
      this.state.nomPh = this.state.pharmacie.nomPh;}
    if (this.state.matricule === '') {
      this.state.matricule = this.state.pharmacie.matricule;}
    if (this.state.ville === '') {
      this.state.ville = this.state.pharmacie.ville;}
    if (this.state.adresse === '') {
      this.state.adresse = this.state.pharmacie.adresse;}
    if (this.state.localisation === '') {
      this.state.localisation = this.state.pharmacie.localisation;}
    if (this.state.email === '') {
      this.state.email = this.state.pharmacie.email;}
    if (this.state.username === '') {
      this.state.username = this.state.pharmacie.username;}
    if (this.state.password === '') {
      this.state.password = this.state.pharmacie.password;}
    if (this.state.tel === '') {
      this.state.tel = this.state.pharmacie.tel;}




    else
    {
      const id=localStorage.getItem('id');
      const nomPh=this.state.nomPh;
      const matricule=this.state.matricule;
      const ville=this.state.ville;
      const adresse=this.state.adresse;
      const localisation=this.state.localisation;
      const password=this.state.password;
      const username=this.state.username;
      const email=this.state.email;
      const tel=this.state.tel;



      const headers={
        "Authorization":"Bearer "+localStorage.getItem("token")
      }
      const data={id,nomPh,matricule,ville,adresse,localisation,tel,email,username,password}
      console.log("modifier votre donnée")
      axios.put("http://localhost:8080/pharmacie/update",data,{headers:headers})
        .then(res=>{
          console.log("data",res.data);

          window.location.href = '/Pharmacie';
        });
    }
  }

  render() {
    return (
      <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
          <div class="login-content">
            <div class="login-logo">
              <a href="index.html">
                <img class="align-content" src="images/logo.png" alt=""/>
              </a>
            </div>
            <div class="modifierpharmacie">
            <div class="login-form">
              <div class="titre"><h1> Modifier Pharmacie</h1></div>
              <div class="form-group">

                <p>NomPharmacie</p>
                <input type="nomPh" class="form-control" placeholder={this.state.pharmacie.nomPh}
                       onChange={evt=> this.setState({nomPh: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>Matricule</p>
                <input type="matricule" class="form-control" placeholder={this.state.pharmacie.matricule}
                       onChange={evt=> this.setState({matricule: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>Ville</p>
                <input type="ville" class="form-control" placeholder={this.state.pharmacie.ville}
                       onChange={evt=> this.setState({ville: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>AdressePh</p>
                <input type="adresse" class="form-control" placeholder={this.state.pharmacie.adresse}
                       onChange={evt=> this.setState({adresse: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>Localisation</p>
                <input type="localisation" class="form-control" placeholder={this.state.pharmacie.localisation}
                       onChange={evt=> this.setState({localisation: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>username</p>
                <input type="localisation" class="form-control" placeholder={this.state.pharmacie.username}
                       onChange={evt=> this.setState({username: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>password</p>
                <input type="password" class="form-control" placeholder={this.state.pharmacie.password}
                       onChange={evt=> this.setState({password: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>email</p>
                <input type="email" class="form-control" placeholder={this.state.pharmacie.email}
                       onChange={evt=> this.setState({email: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>tel</p>
                <input type="tel" class="form-control" placeholder={this.state.pharmacie.tel}
                       onChange={evt=> this.setState({tel: evt.target.value})}/>
              </div>



             <div class="modifier">
               <button type="submit" onClick={this.handleEdit.bind(this)} class="btn btn-primary btn-flat m-b-30 m-t-30">Modifier</button></div>

              <div class="social-login-content">

              </div>
              <div class="register-link m-t-15 text-center">
                <p>Already have account ? <a href="#"> Sign in</a></p>
              </div>

            </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modif;
