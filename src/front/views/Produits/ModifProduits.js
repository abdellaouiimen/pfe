import React, {Component} from 'react';
import axios from 'axios';

class Modif extends Component {

  constructor() {
    super();
    this.state = {
      produits: {
        refernece: '',
        nomProduit: '',
        prix: '',
        datefabrication: '',
        datedexperation: '',

      },
      refernece: '',
      nomProduit: '',
      prix: '',
      datefabrication: '',
      datedexperation: '',

    };


  }
  componentDidMount() {
    console.log('id',localStorage.getItem(('id')));
    this.getone();


  }
  getone() {
    console.log("token ", localStorage.getItem("token"));
    const headers={
      "Authorization":"Bearer "+localStorage.getItem("token")
    }
    fetch('http://localhost:8080/produits/one/' + localStorage.getItem('id'),{method: 'GET', headers: headers})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({produits: data});
      });
  }
  componentWidMount() {
    console.log('will');

  }

  handleEdit() {
    console.log('state: ',this.state);

    if (this.state.refernece === '') {
      this.state.refernece = this.state.produits.refernece;}
    if (this.state.nomProduit === '') {
      this.state.nomProduit = this.state.produits.nomProduit;}
    if (this.state.prix === '') {
      this.state.prix = this.state.produits.prix;}
    if (this.state.datefabrication === '') {
      this.state.datefabrication = this.state.produits.datefabrication;}
    if (this.state.datedexperation === '') {
      this.state.datedexperation = this.state.produits.datedexperation;}





    else
    {
      const id=localStorage.getItem('id');
      const refernece=this.state.refernece;
      const nomProduit=this.state.nomProduit;
      const prix=this.state.prix;
      const datefabrication=this.state.datefabrication;
      const datedexperation=this.state.datedexperation;



      const headers={
        "Authorization":"Bearer "+localStorage.getItem("token")
      }
      const data={id,refernece,nomProduit,prix,datefabrication,datedexperation}
      console.log("modifier votre donnée")
      axios.put("http://localhost:8080/produits/update",data,{headers:headers})
        .then(res=>{
          console.log("data",res.data);

          window.location.href = '/Produits';
        });
    }
  }

  render() {
    return (
      <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
          <div class="login-content">
            <div class="login-logo">
              <a href="index.html">
                <img class="align-content" src="images/logo.png" alt=""/>
              </a>
            </div>
            <div class="modifierpharmacie">
            <div class="login-form">
              <div class="titre"><h1> Modifier Pharmacie</h1></div>
              <div class="form-group">

                <p>nomProduit</p>
                <input type="nomProduit" class="form-control" placeholder={this.state.produits.nomProduit}
                       onChange={evt=> this.setState({nomProduit: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>refernece</p>
                <input type="refernece" class="form-control" placeholder={this.state.produits.refernece}
                       onChange={evt=> this.setState({refernece: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>prix</p>
                <input type="prix" class="form-control" placeholder={this.state.produits.prix}
                       onChange={evt=> this.setState({prix: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>datefabrication</p>
                <input type="datefabrication" class="form-control" placeholder={this.state.produits.datefabrication}
                       onChange={evt=> this.setState({datefabrication: evt.target.value})}/>
              </div>
              <div class="form-group">
                <p>datedexperation</p>
                <input type="datedexperation" class="form-control" placeholder={this.state.produits.datedexperation}
                       onChange={evt=> this.setState({datedexperation: evt.target.value})}/>
              </div>



             <div class="modifier">
               <button type="submit" onClick={this.handleEdit.bind(this)} class="btn btn-primary btn-flat m-b-30 m-t-30">Modifier</button></div>

              <div class="social-login-content">

              </div>
              <div class="register-link m-t-15 text-center">
                <p>Already have account ? <a href="#"> Sign in</a></p>
              </div>

            </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modif;
