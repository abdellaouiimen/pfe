import React, {Component} from 'react';
import axios from 'axios';
import Modal from 'react-responsive-modal';

class Modif extends Component {

  constructor() {
    super();
    this.state = {
      produits: {
        refernece: '',
        nomProduit: '',
        prix: '',
        datefabrication: '',
        datedexperation: '',

        open: false,
      },

    };
  }

  onOpenModal = () => {
    this.setState({open: true});
  };
  onCloseModal = () => {
    this.setState({open: false});
  };


  register() {
    console.log("token ", localStorage.getItem("token"));
    const headers={
      "Authorization":"Bearer "+localStorage.getItem("token")
    }
    console.log("state: ", this.state)
    if (this.state.refernece === "" && this.state.nomProduit === "" && this.state.prix === "" && this.state.datefabrication === ""
      && this.state.datedexperation===""
    ) {
      alert("no data");
    }
    else {
      axios.post("http://localhost:8080/produits/add", {
        nomProduit: this.state.nomProduit,
        refernece: this.state.refernece,
        prix: this.state.prix,
        datefabrication: this.state.datefabrication,
        datedexperation: this.state.datedexperation,

      },{headers:headers}).then(res => {
        console.log(res.data);
        this.onOpenModal();
        window.location.href = "/Produits"
      })
    }
    this.setState({nomProduit: ""})
    this.setState({refernece: ""})
    this.setState({prix: ""})
    this.setState({datefabrication: ""})
    this.setState({datedexperation: ""})


  }


  render() {
    const {open} = this.state;
    return (

        <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
          <div class="login-content">
            <div class="login-logo">
              <a href="index.html">
                <img class="align-content" src="images/logo.png" alt=""/>
              </a>
            </div>
            <div class="ajouterpharmacie">
              <div class="login-form">
                <div><h1>Ajouter Produits</h1></div>
                <div class="form-group">

                  <p>nomProduit</p>
                  <input type="nomProduit" class="form-control" placeholder=" nom"
                         onChange={evt => this.setState({nomProduit: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>refernece</p>
                  <input type="refernece" class="form-control" placeholder="refernece"
                         onChange={evt => this.setState({refernece: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>prix</p>
                  <input type="prix" class="form-control" placeholder="prix"
                         onChange={evt => this.setState({prix: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>dateFabrication</p>
                  <input type="datefabrication" class="form-control" placeholder="datefabrication"
                         onChange={evt => this.setState({datefabrication: evt.target.value})}/>
                </div>
                <div class="form-group">
                  <p>dateExperation</p>
                  <input type="datedexperation" class="form-control" placeholder="datedexperation"
                         onChange={evt => this.setState({datedexperation: evt.target.value})}/>
                </div>



                <div class="btnAjouter">
                  <button type="Ajouter" onClick={this.register.bind(this)}
                          class="btn btn-primary btn-flat m-b-30 m-t-30">Ajouter</button>

                </div>
                <Modal open={open} onClose={this.onCloseModal} right>
                  <td>  <h6 class="div123"> pharmacie ajouté avec succées</h6></td>



                </Modal>
              </div>

            </div>
          </div>
        </div>
      </div>


    );
  }
}

export default Modif;
