// @flow weak

import React, {PureComponent}                       from 'react';

import PropTypes        from 'prop-types';
import { AnimatedView } from '../../components';
import Modal from 'react-responsive-modal';



let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;


class SimpleTables extends PureComponent {
  constructor() {
    super();
    {
      this.state = {
        produits: [],
        open: false,
        id:"",
        currentPage: 1,
        todosPerPage: 5
      };
    }
    this.handleClick = this.handleClick.bind(this);

    this.handleLastClick = this.handleLastClick.bind(this);

    this.handleFirstClick = this.handleFirstClick.bind(this);

  }
  handleClick(event) {

    event.preventDefault();

    this.setState({
      currentPage: Number(event.target.id)
    });
  }



  handleLastClick(event) {

    event.preventDefault();

    this.setState({
      currentPage:last
    });
  }


  handleFirstClick(event) {

    event.preventDefault();

    this.setState({
      currentPage:1
    });
  }

  onOpenModal (e,id) {
    e.preventDefault();
    console.log("id", id)
    this.setState({ id: id });
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  componentWillMount() {
    const { actions: {  enterSimpleTables } } = this.props;
    enterSimpleTables();
  }

  componentWillUnmount() {
    const { actions: {  leaveSimpleTables } } = this.props;
    leaveSimpleTables();
  }

  componentDidMount() {
    this.getAll();
  }

  getAll() {
    console.log("token ", localStorage.getItem("token"));
const headers={
  "Authorization":"Bearer "+localStorage.getItem("token")
}
    fetch('http://localhost:8080/produits/all', {method: 'GET', headers: headers})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({produits: data});
      });
  }
  remove =()=> {

    const headers={
      "Authorization":"Bearer "+localStorage.getItem("token")
    }

    fetch('http://localhost:8080/produits/delete/' +this.state.id, {method: 'DELETE',headers:headers})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        if (data.state === 'non') {
          alert("ce personne n'est pas supprimé");
        }
        else {
          this.onCloseModal();
          this.getAll();

        }

      });


  }
  modif(e,id) {
    e.preventDefault();
    console.log('id',id);
    localStorage.setItem('id',id);
    window.location.href = '/ModifProduits';
  }





  render() {
    const { open } = this.state;
    let {produits, currentPage, todosPerPage} = this.state;


    // Logic for displaying current todos

    let indexOfLastTodo = currentPage * todosPerPage;

    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;

    let currentTodos = produits.slice(indexOfFirstTodo, indexOfLastTodo);


    prev = currentPage > 0 ? (currentPage - 1) : 0;

    last = Math.ceil(produits.length / todosPerPage);

    next = (last === currentPage) ? currentPage : currentPage + 1;



    // Logic for displaying page numbers

    let pageNumbers = [];

    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }


    return (
    <AnimatedView>
      <div className="row">
        <div className="col-xs-12">
          <div className="panel">
            <header className="panel-heading">
              <p class="liste">Liste des produits</p>
            </header>

            <div className="panel-body table-responsive">
              <div className="box-tools m-b-15">
                <div className="input-group">
                  <input
                    type="text"
                    name="table_search"
                    className="form-control input-sm pull-right"
                    style={{width: '150px'}}
                    placeholder="Search"
                  />
                  <div className="input-group-btn">
                    <button className="btn btn-sm btn-default">
                      <i className="fa fa-search"/>
                    </button>
                  </div>
                </div>
              </div>

              <table className="table table-hover">
                <thead>
                <tr>
                  <th>nomProduit</th>
                  <th>refernece</th>
                  <th>prix</th>
                  <th>dateFabrication</th>
                  <th>dateExperation</th>
                  <th>Edit</th>
                  <th>Supprimer</th>


                </tr>
                </thead>
                <tbody>
                {

                  currentTodos.map((item,index) =>{

                    return(

                      <tr key={index}>
                      <td>{item.nomProduit}</td>
                      <td>{item.refernece}</td>
                      <td>{item.prix}</td>
                      <td>{item.datefabrication}</td>
                      <td>{item.datedexperation}</td>

                      <td><i class="fa fa-edit"style={{color:'yellow'}} onClick={e => this.modif(e, item.id)} /></td>
                      <td ><i onClick={e=>this.onOpenModal(e,item.id)} class="fa fa-remove"style={{color:'red'}} />

                        <Modal open={open} onClose={this.onCloseModal} right>
                     <div>
                       <td ><img src="https://img2.freepng.fr/20180410/xew/kisspng-error-computer-icons-error-5acc81745e2a90.7205482315233519243857.jpg" width="50" height="50"/></td>
                             <td>  <h6 class="div123">voulez vous supprimer cette pharmacie!</h6></td>

                     </div>





                          <button type="button" class="Button" onClick={this.remove}>supprimer </button>
                          <button type="button"  class="Button"onClick={this.onCloseModal}> Annuler</button>
                      </Modal>
                      </td>
                    </tr>
                    );

                  })

                }

                </tbody>
              </table>

              </div>
            <div className="table-foot">
              <ul className="pagination">
                <li className={prev === 0 ? 'disabled' : ''}>
                  <a onClick={this.handleFirstClick} id={prev} href={prev}>First</a>
                </li>
                <li className={prev === 0 ? 'disabled' : ''}>
                  <a onClick={this.handleClick} id={prev} href={prev}>Previous</a>
                </li>
                {pageNumbers.map((page, index) =>
                  <li key={index} className={pageNumbers[currentPage-1] === page ? 'active' : ''}>
                    <a PaginationLink onClick={this.handleClick} href={page} key={page} id={page}>{page}</a>
                  </li>
                )}
                <li className={currentPage === last ? 'disabled' : ''}>
                  <a onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Next</a>
                </li>
                <li className={currentPage === last? 'disabled' : ''}>
                  <a  onClick={this.handleLastClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Last</a>
                </li>
              </ul>



            </div>
            </div>
          </div>
        </div>

    </AnimatedView>
    );
  }
}

SimpleTables.propTypes = {
  actions: PropTypes.shape({
    enterSimpleTables: PropTypes.func,
    leaveSimpleTables: PropTypes.func,
  }),
};

export default SimpleTables;
