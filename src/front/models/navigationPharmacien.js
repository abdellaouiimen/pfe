export const navigationPharmacien = {
  brand:      'reactDirectorAdmin',
  leftLinks:  [],
  rightLinks: [
    {
      label:      'Home',
      link:       '/',
      view:       'home',
      isRouteBtn: true
    },
    {
      label:      'About',
      link:       '/about',
      view:       'about',
      isRouteBtn: true
    }
  ],
  sideMenu: [
    // group menu #1

    /////id 2
    {
      id: 2,
      group: 'Clients  ',
      menus: [
        {
          name: 'Liste Clients',
          linkTo: '/Clients',
          faIconName: 'fa fa-list'
        },
      ]
    },

    {
      id: 3,
      group: 'Produits  ',
      menus: [
        {
          name: 'Liste Produits',
          linkTo: '/Produits',
          faIconName: 'fa fa-list'
        },
        {
          name: 'Ajout Produits',
          linkTo: '/AjouterProduits',
          faIconName: 'fa fa-plus-circle'
        }
      ]
    }


  ]
};
