export const navigation = {
  brand:      'reactDirectorAdmin',
  leftLinks:  [],
  rightLinks: [
    {
      label:      'Home',
      link:       '/',
      view:       'home',
      isRouteBtn: true
    },
    {
      label:      'About',
      link:       '/about',
      view:       'about',
      isRouteBtn: true
    }
  ],
  sideMenu: [
    // group menu #1
    {
      id: 1,
      group: 'Pharmacie  ',
      menus: [
        {
          name: 'Liste Pharmacie',
          linkTo: '/Pharmacie',
          faIconName: 'fa fa-list'
        },
        {
          name: 'Ajout Pharmacie',
          linkTo: '/Ajouter',
          faIconName: 'fa fa-plus-circle'
        }
      ]
    },
    /////id 2
    {
      id: 2,
      group: 'Clients  ',
      menus: [
        {
          name: 'Liste Clients',
          linkTo: '/Clients',
          faIconName: 'fa fa-list'
        },
      ]
    },
    ///////id 3
    {
      id: 3,
      group: 'Reclamation  ',
      menus: [
        {
          name: 'Liste Reclamation',
          linkTo: '/Reclamation',
          faIconName: 'fa fa-list'
        },
      ]
    },


  ]
};
