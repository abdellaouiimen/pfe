import { navigation } from './navigation';
import { navigationPharmacien } from './navigationPharmacien';
import { earningGraphMockData } from './earningGraphMockData';
import { userInfosMockData } from './userInfosMock';
import { teamMatesMock } from './teamMatesMock';

export {
  navigation,
  earningGraphMockData,
  userInfosMockData,
  teamMatesMock,
  navigationPharmacien
};
